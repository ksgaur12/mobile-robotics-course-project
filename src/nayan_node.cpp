
#include "ros/ros.h"
#include "nav_msgs/Path.h"
#include "nav_msgs/Odometry.h"
#include "geometry_msgs/Pose.h"
#include <tf/transform_broadcaster.h>

nav_msgs::Path quad_path;
nav_msgs::Path quad_kalman_path;
nav_msgs::Path quad_kalman_path_rel;
nav_msgs::Path quad_px4_int_path;
nav_msgs::Path quad_accel_int_path;
nav_msgs::Path quad_svo_path;
nav_msgs::Path Astar_path;
nav_msgs::Path quad_kalam_path_relative;
nav_msgs::Path quad_imu_int_path;

int pose_i=0;
int pose_kalman_i = 0;

void getsvo(const geometry_msgs::Pose::ConstPtr& data){

	quad_svo_path.poses.resize(pose_i+1);


	quad_svo_path.header.seq = pose_i;
	quad_svo_path.header.frame_id = "/world";
	quad_svo_path.header.stamp = ros::Time::now();

	quad_svo_path.poses[pose_i].pose = *data;
	quad_svo_path.poses[pose_i].pose.position.z = 2;
	quad_svo_path.poses[pose_i].header = quad_svo_path.header;
	quad_svo_path.poses[pose_i].header.frame_id = "/world";


}

void getNayanKalmanPose(const geometry_msgs::Pose::ConstPtr& kalman_pose_data){

	quad_kalman_path.poses.resize(pose_i+1);


	quad_kalman_path.header.seq = pose_i;
	quad_kalman_path.header.frame_id = "/world";
	quad_kalman_path.header.stamp = ros::Time::now();

	quad_kalman_path.poses[pose_i].pose = *kalman_pose_data;
	quad_kalman_path.poses[pose_i].pose.position.z = 2;
	quad_kalman_path.poses[pose_i].header = quad_kalman_path.header;
	quad_kalman_path.poses[pose_i].header.frame_id = "/world";

}

void getNayanPx4int(const geometry_msgs::Pose::ConstPtr& kalman_px4_int_data){

	quad_px4_int_path.poses.resize(pose_i+1);


	quad_px4_int_path.header.seq = pose_i;
	quad_px4_int_path.header.frame_id = "/world";
	quad_px4_int_path.header.stamp = ros::Time::now();

	quad_px4_int_path.poses[pose_i].pose = *kalman_px4_int_data;
	quad_px4_int_path.poses[pose_i].pose.position.z = 0;
	quad_px4_int_path.poses[pose_i].header = quad_px4_int_path.header;
	quad_px4_int_path.poses[pose_i].header.frame_id = "/world";

}

void getIMUint(const geometry_msgs::Pose::ConstPtr& kalman_px4_int_data){

	quad_imu_int_path.poses.resize(pose_i+1);


	quad_imu_int_path.header.seq = pose_i;
	quad_imu_int_path.header.frame_id = "/world";
	quad_imu_int_path.header.stamp = ros::Time::now();

	quad_imu_int_path.poses[pose_i].pose = *kalman_px4_int_data;
	quad_imu_int_path.poses[pose_i].pose.position.z = 0;
	quad_imu_int_path.poses[pose_i].header = quad_px4_int_path.header;
	quad_imu_int_path.poses[pose_i].header.frame_id = "/world";

}

void getAstarPath(const geometry_msgs::Pose::ConstPtr& data){

	Astar_path.poses.resize(pose_i+1);


	Astar_path.header.seq = pose_i;
	Astar_path.header.frame_id = "/world";
	Astar_path.header.stamp = ros::Time::now();

	Astar_path.poses[pose_i].pose = *data;
	Astar_path.poses[pose_i].pose.position.z = 0;
	Astar_path.poses[pose_i].header = quad_kalman_path_rel.header;
	Astar_path.poses[pose_i].header.frame_id = "/world";

}

void getsvoPose(const geometry_msgs::PoseStamped::ConstPtr& data){

	quad_svo_path.poses.resize(pose_i+1);


	quad_svo_path.header.seq = pose_i;
	quad_svo_path.header.frame_id = "/world";
	quad_svo_path.header.stamp = ros::Time::now();

	quad_svo_path.poses[pose_i].pose = data->pose;
	quad_svo_path.poses[pose_i].pose.position.z = 0;
	quad_svo_path.poses[pose_i].header = quad_kalman_path_rel.header;
	quad_svo_path.poses[pose_i].header.frame_id = "svo_nayan";

	 static tf::TransformBroadcaster br;
	 tf::Transform transform;
	 transform.setOrigin( tf::Vector3(data->pose.position.x, data->pose.position.y, 2.0) );
	 tf::Quaternion q;
	 q.setRPY(0, 0, 0);
	 transform.setRotation(q);
	 br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "world", "svo_nayan"));


}


int main(int argc, char **argv){

	ros::init(argc, argv,"nayan_node");

	ros::NodeHandle n;

	ros::NodeHandle nayan_kalman_pose_handle;
	ros::Subscriber nayan_pose_kalman_sub = nayan_kalman_pose_handle.subscribe("nayan/kalman/pose", 1000,getNayanKalmanPose);

	ros::NodeHandle nayan_rviz_pose_handle;
	ros::Subscriber nayan_pose_rviz_sub = nayan_rviz_pose_handle.subscribe("nayan/Astar/path",1000,getAstarPath);
	ros::Subscriber nayan_pose_px4_int_sub = n.subscribe("nayan/px4/path",1000,getNayanPx4int);
	ros::Subscriber nayan_pose_imu_int_sub = n.subscribe("nayan/imu/int/path",1000,getIMUint);
	ros::Subscriber nayan_pose_vision_sub = n.subscribe("mavros/vision_pose/pose",1000,getsvoPose);

	ros::Publisher nayan_astar_path_pub = n.advertise<nav_msgs::Path>("nayan/rviz/Astar/path",1000);
	ros::Publisher nayan_kalman_path_pub = n.advertise<nav_msgs::Path>("nayan/rviz/Kalman/path",1000);
	ros::Publisher nayan_px4flow_path_pub = n.advertise<nav_msgs::Path>("nayan/rviz/px4flow/path",1000);
	ros::Publisher nayan_IMU_path_pub = n.advertise<nav_msgs::Path>("nayan/rviz/IMU/path",1000);
	ros::Publisher nayan_svo_path_pub = n.advertise<nav_msgs::Path>("nayan/rviz/svo/path",1000);

	ros::Rate loop_rate(20);

	ros::Time begin = ros::Time::now();

	while(ros::ok()){
		ros::spinOnce();
		loop_rate.sleep();
		nayan_astar_path_pub.publish(Astar_path);
		nayan_kalman_path_pub.publish(quad_kalman_path);
		nayan_px4flow_path_pub.publish(quad_px4_int_path);
		nayan_IMU_path_pub.publish(quad_imu_int_path);
		nayan_svo_path_pub.publish(quad_svo_path);
		pose_i++;
	}
	return 0;

}
