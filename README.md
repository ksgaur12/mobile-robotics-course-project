**EE698 Probabilistic Mobile Robotics Course Project**
#**  Path Planning  **#

This code implements astar path planning algorithm and kalman filter localization using px4flow optical camera and IMU data.

##** Disclaimer **##

Path Planning has been tested under ROS Indigo with Ubuntu 14.04. The code has been implement on a Quadrotor platform, Nayan(http://aus.co.in/products.html).

##** Dependencies **##

Path Planning Package depends on the following packages.

1. [cv_sense](https://github.com/atulyashivamshree/cv_sens)
2. [gazebo_msgs](http://wiki.ros.org/gazebo_msgs)
3. [mavros](http://wiki.ros.org/mavros)
4. [Px4flow](http://wiki.ros.org/px4flow_node)
5. [Hector_quadrotor](http://wiki.ros.org/hector_quadrotor)
6. [px_ros_pkg](https://github.com/cvg/px-ros-pkg)

##** ROS Installation **##

Open the terminal and type:


```
#!txt
$ sudo apt-get install ros-indigo-gazebo-msgs

```

```
#!txt
$ sudo apt-get intall ros-indigo-mavros

```

For cv_sense, px_ros_pkg, hector_quadcopter follow the ros wiki.

Clone the Mobile Robotics Course project in your catkin workspace and catkin_make the workspace.

###  path_planning catkin ###

```
#!txt
$ cd catkin_ws/src
$ git clone https://ksgaur12@bitbucket.org/ksgaur12/mobile-robotics-course-project.git
$ cd ..
$ catkin_make
```

## ** Setup for On-board Computer** 

```
#!txt

Before running the code on the OBC, make sure that all the packages are correctly installed.
```
*  Launching the nayan_slave for communication between OBC and HLP.

  Open a terminal
```
#!txt

  $ roslaunch cv_sens nayan_slave.launch
```

  Test that the topic mavors/imu/data is publishing the quaternion and accelerometer data.

* Run the px4flow node.

  Open another terminal.

```
#!txt

  $ roslaunch px4flow px4flow.launch
```

* Run the talker node of path planning.


```
#!txt

 $ rosrun path_planning talker.py
```

* Launch the cv_sens node.


```
#!txt

$ roslaunch cv_sens sensor_fusion.launch
```

## Visualization and Simulation on RVIZ and gazebo ##

First, install the gazebo for ROS.


```
#!txt
$ sudo apt-get install ros-indigo-gazebo-ros ros-indigo-gazebo-concert ros-indigo-gazebo-control ros-indigo-gazebo-plugins ros-indigo-gazebo-ros-pkgs ros-indigo-gazebo-ros-control
$ sudo apt-get install ros-indigo-rviz

```
Run the launch file.


```
#!txt

$ roslaunch path_planning test1.launch
```