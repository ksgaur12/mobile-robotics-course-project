#!/usr/bin/env python

import astar
import rospy
import PID_controller
import tf
import numpy as np
import Initialize_path
import constraint
import math
import ECEFtoNED
import bftoefTransformation
import kalman

from path_planning.msg import goal
from path_planning.msg import grid
from path_planning.msg import PID
from path_planning.msg import Kal
from path_planning.msg import Kal2

from nav_msgs.msg import GridCells
from geometry_msgs.msg import Point
from gazebo_msgs.msg import ModelState
from sensor_msgs.msg import NavSatFix
from sensor_msgs.msg import Imu
from px_comm.msg import OpticalFlow
from nav_msgs.msg import Path
from nav_msgs.msg import Odometry
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import Twist
from geometry_msgs.msg import Pose
from geometry_msgs.msg._Vector3Stamped import Vector3Stamped
from geometry_msgs.msg import Vector3
from mavros_msgs.msg import RCIn

nayan_GPS_pose = goal()
nayan_Kalman_pose = goal()
nayan_current_pose = goal()
nayan_twist = Twist()    
nayan_path_rviz = Odometry()
nayan_path_GPS_rviz = Odometry()
grid_map = GridCells()
points = Point()
Nayan_state = ModelState()
pose_PID_x = PID()
pose_PID_y = PID()
pose_PID_z = PID()
NayanImuDatabf = Imu()
NayanImuDataef = Imu()
NayanOptflowDatabf = OpticalFlow()
NayanOptflowDataef = OpticalFlow()
NayanGPSPath = Path()
NayanAstarPath = Path()
kalman_x = Kal()
kalman_y = Kal()
kal_x = Kal2()
kal_y = Kal2()
desired_pose = PoseStamped()
kalman_pose = Pose()
accelef = Vector3()
velef = Vector3()
RPY = Vector3()
nayan_svo_pose = Pose()
px4_integrated_velocity = Pose()
nayan_astar_pose = Pose()
rviz_pose = Pose()
imu_accel_int = Pose()
nayan_svo_pose = Pose()

kal_prev_x = 0.0
kal_prev_y = 0.0
imu_accel_int.position.x = 0.0
imu_accel_int.position.y = 0.0

current_time = 0.0
previous_time = 0.0
imu_vel_x = 0.0
imu_vel_y = 0.0
dt = 0.0
x = 0.0
y = 0.0
switch_channel = 0
channel_ok = 0
kalmanState_x = np.array([[0],[0]])
kalmanState_y = np.array([[0],[0]])
covariance_x = np.array([[0,0],[0,0]])
covariance_y = np.array([[0,0],[0,0]])


pid_param = 2

path_count = 1
astar_path_count = 1
secquence = 1
frequency = 20
count=0
kalman_count =0
accelVariance_x =0.007868
accelVariance_y =0.008712
sensorVariance_x = 0.006017
sensorVariance_y = 0.00995

robot_frame_id = 'Nayan'
robot_GPS_id = 'Nayan_GPS'
path = astar.search()
path_estimate1 = []
next_ok = 1


path_estimate1.append(path[0])
for i in range (len(path)-1):
	a = path[i+1][0] - path[i][0]
	b = path[i+1][1] - path[i][1]		 
	path_estimate1.append([a,b])

point = Point() 
#print path_estimate1

def GetImuData(data):
	global NayanOptflowDataef, NayanImuDataef, NayanOptflowDatabf, kalman_pose, accelef, velef, RPY, imu_vel_x, imu_vel_y, imu_accel_int

	NayanImuDatabf = data
	
	eulerAngles = np.array(bftoefTransformation.QuaterniontoRPY(NayanImuDatabf.orientation.w,NayanImuDatabf.orientation.x,NayanImuDatabf.orientation.y,NayanImuDatabf.orientation.z))	
	
	RPY.x = eulerAngles[0]
	RPY.y = eulerAngles[1]
	RPY.z = eulerAngles[2]
	
	kalman_pose.position.z = eulerAngles[2]
	RotationMatrix = np.array(bftoefTransformation.RotationMatrixfromquaternion(NayanImuDatabf.orientation.w,NayanImuDatabf.orientation.x,NayanImuDatabf.orientation.y,NayanImuDatabf.orientation.z))
	
	accelValuesbf = np.array([[NayanImuDatabf.linear_acceleration.x],[NayanImuDatabf.linear_acceleration.y],[NayanImuDatabf.linear_acceleration.z]])
	accelValuesef = np.array(bftoefTransformation.bftoef(accelValuesbf,RotationMatrix))
	
	imu_accel_int.position.x = imu_accel_int.position.x + imu_vel_x*1.0/frequency + 0.5*accelValuesef[0]*1.0/(frequency*frequency);
	imu_accel_int.position.y = imu_accel_int.position.y + imu_vel_y*1.0/frequency + 0.5*accelValuesef[1]*1.0/(frequency*frequency);	 
	imu_vel_x = imu_vel_x + accelValuesef[0]*1.0/frequency;
	imu_vel_y = imu_vel_y + accelValuesef[1]*1.0/frequency;
	
	NayanImuDataef = data
	NayanImuDataef.linear_acceleration.x = accelValuesef[0]
	NayanImuDataef.linear_acceleration.y = accelValuesef[1]
	NayanImuDataef.linear_acceleration.z = accelValuesef[2]
	
	accelef = NayanImuDataef.linear_acceleration


	px4flowVelocitybf = np.array([[NayanOptflowDatabf.velocity_y],[NayanOptflowDatabf.velocity_x],[0]])
 	px4flowVelocityef = np.array(bftoefTransformation.bftoef(px4flowVelocitybf,RotationMatrix))

 	NayanOptflowDataef.velocity_x = px4flowVelocityef[0]
 	NayanOptflowDataef.velocity_y = px4flowVelocityef[1]
 	
 	velef.x = NayanOptflowDataef.velocity_x
 	velef.y = NayanOptflowDataef.velocity_y
 	
 	
def GetOpticalFlowData(data):	
	global NayanOptflowDataef, NayanOptflowDatabf
	NayanOptflowDatabf = data

def GetsvoPose(data):
	global nayan_current_pose, nayan_svo_pose
	nayan_svo_pose.position.x = data.pose.position.x
	nayan_svo_pose.position.y = data.pose.position.y
	nayan_svo_pose.position.z = 2
	nayan_current_pose.nayan_pose.position.x = data.pose.position.x
	nayan_current_pose.nayan_pose.position.y = data.pose.position.y
	nayan_current_pose.nayan_pose.position.z = data.pose.position.z
	
def GetRCvalues(data):
	global switch_channel
	switch_channel = data.channels[6]
	
def GetPIDParam():

    pose_PID_x.Kp = 2.0
    pose_PID_x.Ki = 2.0
    pose_PID_x.Kd = 0.1
    pose_PID_x.previousTime = 0.0
    pose_PID_x.lastError = 0.0
    pose_PID_x.integratedError = 0.0  

    pose_PID_y.Kp = 2.0 
    pose_PID_y.Ki = 2.0
    pose_PID_y.Kd = 0.1
    pose_PID_y.previousTime = 0.0
    pose_PID_y.lastError = 0.0
    pose_PID_y.integratedError = 0.0

    pose_PID_z.Kp = 2.0 
    pose_PID_z.Ki = 1.0
    pose_PID_z.Kd = 1.0
    pose_PID_z.previousTime = 0.0
    pose_PID_z.lastError = 0.0
    pose_PID_z.integratedError = 0.0
     
def constraint(a,b,c):
	if a<b:
		a = b
	if a>c:
		a= c
	return a

def caltargetdiff():
    global path_count, pose, NayanAstarPath, robot_frame_id, prevpose, pose_PID_x, pose_PID_y 
    global secquence, desired_pose, nayan_current_pose, path_estimate1, next_ok, kalman_pose, channel_ok, switch_channel, rviz_pose

    delta_time = 1.0/20

    
    desired_pose.pose.orientation.w = 1.0
    desired_pose.pose.orientation.x = 0.0
    desired_pose.pose.orientation.y = 0.0
    desired_pose.pose.orientation.z = 0.0
    
    if switch_channel > 1500 and channel_ok is 0:
    	nayan_current_pose.nayan_pose.position.x = kalman_pose.position.x
    	nayan_current_pose.nayan_pose.position.y = kalman_pose.position.y
    	channel_ok = 1
     	print switch_channel
    if switch_channel<1500 and channel_ok is 1:
    	channel_ok = 0

	rviz_pose.position.x = kalman_pose.position.x - nayan_current_pose.nayan_pose.position.x + path_estimate1[path_count-1][0]	
	rviz_pose.position.y = kalman_pose.position.y - nayan_current_pose.nayan_pose.position.y + path_estimate1[path_count-1][1]    		
    
    radius_value = ((kalman_pose.position.x - nayan_current_pose.nayan_pose.position.x)  - path_estimate1[path_count][0])**2 + ((kalman_pose.position.y - nayan_current_pose.nayan_pose.position.y) - path_estimate1[path_count][1])**2
    if radius_value < 0.09 and len(path) is not (path_count+1):	
    	path_count += 1
    	next_ok = 1
    	channel_ok = 0

    
    desired_pose.pose.position.x = path_estimate1[path_count][0]
    desired_pose.pose.position.y = path_estimate1[path_count][1]
    desired_pose.pose.position.z = 0.0	
    
    print desired_pose.pose.position.x, desired_pose.pose.position.y
    
    nayan_path_rviz.pose.pose = nayan_current_pose.nayan_pose
    nayan_path_rviz.twist.twist =  nayan_twist
    nayan_path_rviz.header.frame_id = robot_frame_id
   

def GetastarPath():
    global path_count, astar_path_count, pose, NayanAstarPath, robot_frame_id, prevpose, pose_PID_x, pose_PID_y 
    global secquence, desired_pose , nayan_astar_pose, path_estimate1, next_ok, kalman_pose, channel_ok, switch_channel
    
    delta_time = 1.0/frequency
	
    [nayan_twist.linear.z, pose_PID_z.integratedError, pose_PID_z.lastError] = PID_controller.PID(nayan_astar_pose.position.z,2,pose_PID_z,delta_time)
    nayan_astar_pose.position.z += delta_time * nayan_twist.linear.z

    if nayan_astar_pose.position.z > 1.8:
    	[nayan_twist.linear.x, pose_PID_x.integratedError, pose_PID_x.lastError] = PID_controller.PID(nayan_astar_pose.position.x,path[astar_path_count][0],pose_PID_x,delta_time)
    	[nayan_twist.linear.y, pose_PID_y.integratedError, pose_PID_y.lastError] = PID_controller.PID(nayan_astar_pose.position.y,path[astar_path_count][1],pose_PID_y,delta_time)

	nayan_twist.linear.x = constraint(nayan_twist.linear.x,-0.5,0.5)
	nayan_twist.linear.y = constraint(nayan_twist.linear.y,-0.5,0.5)
	
	nayan_astar_pose.position.x += delta_time * nayan_twist.linear.x
	nayan_astar_pose.position.y += delta_time * nayan_twist.linear.y

    radius_value = (nayan_astar_pose.position.x  - path[astar_path_count][0])**2 + (nayan_astar_pose.position.y - path[astar_path_count][1])**2
    if radius_value < 0.09 and len(path) is not (astar_path_count+1):	
    	astar_path_count += 1
    	
		
	Nayan_state.model_name = 'quadrotor'
	Nayan_state.twist = nayan_twist
	Nayan_state.pose = nayan_astar_pose
	Nayan_state.reference_frame = 'world'


def broadcast_robot_frame():
    br = tf.TransformBroadcaster()
    br.sendTransform((nayan_current_pose.nayan_pose.position.x, nayan_current_pose.nayan_pose.position.y, nayan_current_pose.nayan_pose.position.z),tf.transformations.quaternion_from_euler(0, 0, 0),rospy.Time.now(),robot_frame_id,"world")
    br1 = tf.TransformBroadcaster()
    br1.sendTransform((kalman_pose.position.x, kalman_pose.position.y,0),tf.transformations.quaternion_from_euler(0, 0, 0),rospy.Time.now(),"Nayan_Kalman","world")

def create_map():
	
    grid_map.cell_width = 1.0
    grid_map.cell_height = 1.0
    
    del grid_map.cells[:]
    grid_map.header.frame_id = 'world'
    for i in range(len(astar.grid)):
    	for j in range(len(astar.grid[0])):
	        if astar.grid[i][j] == 1:
	            grid_map.cells.append(Point(i,j,2))    
    	 	    #print i,j, grid_map.cells

def KF():
	global kalman_x, kalman_y, covariance_x, covariance_y, NayanImuDataef,NayanOptflowDataef, kalman_pose, kalman_count, x, y, kal_prev_x, kal_prev_y
	if kalman_count is 0:

		kalman_x.state = kalmanState_x
		kalman_x.covariance = covariance_x

		sensorCovariance_x = kalman.KalmanSensorVariance(sensorVariance_x)

		kalman_y.state = kalmanState_y
		kalman_y.covariance = covariance_y
		
		sensorCovariance_y = kalman.KalmanSensorVariance(sensorVariance_y)

		
	else :
		kalman_x = kalman.KalmanPrediction(kalman_x.state,NayanImuDataef.linear_acceleration.x,NayanOptflowDataef.velocity_x, 1.0/frequency,kalman_x.covariance,accelVariance_x,sensorVariance_x)
		kalman_y = kalman.KalmanPrediction(kalman_y.state,NayanImuDataef.linear_acceleration.y,NayanOptflowDataef.velocity_y, 1.0/frequency,kalman_y.covariance,accelVariance_y,sensorVariance_y)
		

	kalman_pose.position.x = (kalman_x.state[0])*2 
	kalman_pose.position.y = (kalman_y.state[0])*2
	kalman_count = kalman_count + 1	
	if channel_ok is 1:
		kal_prev_x = kalman_pose.position.x
		kal_prev_y = kalman_pose.position.y
	#print 'x',kalman_x.state[0]
	#print 'y', kalman_y.state[0]
	
def px4_flow_alone():
    global px4_integrated_velocity
    px4_integrated_velocity.position.x = px4_integrated_velocity.position.x + 1.0/frequency * NayanOptflowDataef.velocity_x	
    px4_integrated_velocity.position.y = px4_integrated_velocity.position.y + 1.0/frequency * NayanOptflowDataef.velocity_y	
    px4_integrated_velocity.position.z = 2
   
def Nayan():
	global switch_channel
	GetPIDParam()
	broadcast_robot_frame()
	KF()
	caltargetdiff()
	GetastarPath()
	create_map()
	px4_flow_alone()

def talker():
	
    global next_ok
    Robot_pose_pub = rospy.Publisher('nayan/pose',goal,queue_size=10)
    Robot_svo_pose_pub = rospy.Publisher('nayan/svo/pose',Pose,queue_size=10)
    Robot_px4_pose_pub = rospy.Publisher('nayan/px4/path',Pose,queue_size=10)
    Robot_path_pub = rospy.Publisher('nayan/path',Odometry,queue_size=10)	
    Robot_grid_pub = rospy.Publisher('nayan/environment',GridCells,queue_size=10)
    Robot_state_pub = rospy.Publisher('gazebo/set_model_state',ModelState,queue_size=10)
    Robot_kalman_pose_pub = rospy.Publisher('nayan/kalman/pose',Pose,queue_size=10)
    Robot_accel_ef_pub = rospy.Publisher('nayan/accel/ef',Vector3,queue_size=10)
    Robot_vel_ef_pub = rospy.Publisher('nayan/vel/ef',Vector3,queue_size=10)
    Robot_desired_position_pub = rospy.Publisher('mavros/setpoint_position/local',PoseStamped,queue_size=10)
    Robot_rviz_pose_pub = rospy.Publisher('nayan/rviz/pose',Pose, queue_size=10)
    Robot_rviz_Astar_path_pub = rospy.Publisher('nayan/Astar/path',Pose,queue_size=10)
    Robot_rviz_imu_int_path_pub = rospy.Publisher('nayan/imu/int/path',Pose,queue_size=10)
	
    Robot_Imu_sub = rospy.Subscriber('mavros/imu/data',Imu,GetImuData)
    Robot_px4flow_sub = rospy.Subscriber('px4flow/opt_flow',OpticalFlow,GetOpticalFlowData)
    Robot_rc_sub = rospy.Subscriber('mavros/rc/in',RCIn,GetRCvalues)


    rospy.init_node('talker', anonymous=True)	
    rate = rospy.Rate(frequency) 
    Initialize_path.initialize_position()	

    while not rospy.is_shutdown():
    	Nayan()
	Robot_pose_pub.publish(nayan_current_pose)
	Robot_svo_pose_pub.publish(nayan_svo_pose)
	Robot_px4_pose_pub.publish(px4_integrated_velocity)
	Robot_path_pub.publish(nayan_path_rviz)
	Robot_state_pub.publish(Nayan_state)
	Robot_grid_pub.publish(grid_map)  
	Robot_rviz_Astar_path_pub.publish(nayan_astar_pose)
	Robot_rviz_imu_int_path_pub.publish(imu_accel_int)
	
	if switch_channel>1500:
		Robot_rviz_pose_pub.publish(rviz_pose)
		
	Robot_kalman_pose_pub.publish(kalman_pose)
		
	if next_ok is 1 and switch_channel>1500:
		for i in range(3):
			Robot_desired_position_pub.publish(desired_pose)
			next_ok = 0	
	
	rate.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
