import numpy as np
import math as m
from path_planning.msg import Kal


def KalmanInitializestate(initial_x,initial_v):
    x = initial_x
    v = initial_v
    return np.array([[x],[v]])

def KalmanInitializeStateCovariance(C11, C22):
    return np.array([[C11,0.0],[0.0,C22]])

def KalmanAccelVariance(accelError):
    return np.array([accelError])

def KalmanSensorVariance(sensorError):
    return np.array([sensorError])

def KalmanPrediction(state,accelValue,sensorValue, dt,C,accelVariance,Q):
    #print state,accelValue,sensorValue
    A = np.array([[1.0,dt],[0.0,1.0]])
    B = np.array([[dt*dt/2.0],[dt]])
    u =accelValue
    X = state
    X_= np.dot(A,X)+ B*u
    C_= np.dot(np.dot(A,C),np.transpose(A))+ np.dot(np.dot(B,np.array(accelVariance)),np.transpose(B))
    return KalmanUpdate(X_, C_, Q,sensorValue)

def KalmanUpdate(X_,C_,Q,sensorValue):
    z_kal = Kal()
    H = np.array([0.0,1.0])
    Z_ = np.dot(H,X_)
    Z = sensorValue
    S = np.dot(np.dot(H,C_),np.transpose(H))+np.array(Q)
    K = np.dot(C_,np.transpose(H))/S
    K = np.array([[K[0]],[K[1]]])
    I = np.array([[1.0,0.0],[0.0,1.0]])
    X = X_ + K*(Z-Z_)   
    C = np.dot((I-np.array([[0,K[0,0]],[0,K[1,0]]])),C_)
    z_kal.state = X
    z_kal.covariance = C
#    print z_kal
    return z_kal
    