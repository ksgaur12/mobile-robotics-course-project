import numpy as np
from math import atan2
from math import asin

def normalize(q0,q1,q2,q3):
    return np.sqrt(q0*q0+q1*q1+q2*q2+q3*q3)

def QuaterniontoRPY(q0,q1,q2,q3):
    n = normalize(q0,q1,q2,q3)
    q0 = q0/n
    q1 = q1/n
    q2 = q2/n
    q3 = q3/n
    Roll = atan2(2*(q0*q1+q2*q3),1-2*(q1*q1+q2*q2))
    Pitch = asin(2*(q0*q2-q3*q1))
    Yaw = atan2(2*(q0*q3+q1*q2), 1-2*(q2*q2+q3*q3))
    return np.array([Roll,Pitch,Yaw])
    
def RotationMatrixfromquaternion(q0,q1,q2,q3):
    n = normalize(q0,q1,q2,q3)
    q0 = q0/n
    q1 = q1/n
    q2 = q2/n
    q3 = q3/n
        
    a11 = 1-2*q2*q2-2*q3*q3
    a12 = 2*(q1*q2-q0*q3)
    a13 = 2*(q0*q2+q1*q3)
    
    a21 = 2*(q1*q2+q0*q3)
    a22 = 1-2*q1*q1-2*q3*q3
    a23 = 2*(q2*q3-q0*q1)
    
    a31 = 2*(q1*q3-q0*q2)
    a32 = 2*(q2*q3+q1*q0)
    a33 = 1-2*q1*q1-2*q2*q2
    
    RotationMatrix = np.array([[a11,a12,a13],[a21,a22,a23],[a31,a32,a33]]) 
    return RotationMatrix

def bftoef(a,RotationMatrix):
    #print a, RotationMatrix
    return np.dot(RotationMatrix,a)        