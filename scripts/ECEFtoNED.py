import math
import numpy as np
from sensor_msgs.msg import NavSatFix

Rea = 6378137.0 
f = 1.0/298.257223563
Reb = 6356752.0
e = 0.08181919

def intializeNED(GPSdata):
	Cx = math.cos(math.radians(GPSdata.longitude))
	Cy = math.cos(math.radians(GPSdata.latitude))
	Sx = math.sin(math.radians(GPSdata.longitude))
	Sy = math.sin(math.radians(GPSdata.latitude))
	h = GPSdata.altitude
	Me = Rea*(1-pow(e,2))/pow(1-pow(e,2)*pow(Sy,2),1.5)
	Ne = Rea/pow(1-pow(e,2)*pow(Sy,2),0.5)
	Pref = np.array([[(Ne+h)*Cy*Cx],[(Ne+h)*Cy*Sx],[(Ne*(1-e*e)+h)*Sy]])
	
#	print Pref
	return Pref

def initializeTransformECEFtoNED(GPSdata):
	Cx = math.cos(math.radians(GPSdata.longitude))
	Cy = math.cos(math.radians(GPSdata.latitude))
	Sx = math.sin(math.radians(GPSdata.longitude))
	Sy = math.sin(math.radians(GPSdata.latitude))
	Rne = np.array([[-Sy*Cx,-Sy*Sx,Cy],[-Sx,Cx,0],[-Cy*Cx,-Cy*Sx,-Sy]])
	return Rne

def ECEFtoNEDstate(latitude,longitude,altitude,Pref,Rne):
	Me = Rea*(1-pow(e,2))/pow(1-pow(e,2)*pow(math.sin(math.radians(latitude)),2),1.5)
	Ne = Rea/pow(1-pow(e,2)*pow(math.sin(math.radians(latitude)),2),0.5)
	h  = altitude
	Cx = math.cos(math.radians(longitude))
	Cy = math.cos(math.radians(latitude))
	Sx = math.sin(math.radians(longitude))
	Sy = math.sin(math.radians(latitude))
	Pe = np.array([[(Ne+h)*Cy*Cx],[(Ne+h)*Cy*Sx],[(Ne*(1-e*e)+h)*Sy]])
	Pn = np.dot(Rne,(Pe-Pref))
	#print Pe, Pref
	return Pn


